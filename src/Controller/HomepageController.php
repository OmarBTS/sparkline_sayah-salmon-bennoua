<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use App\Form\UploadType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
Use App\Entity\Upload;
use Symfony\Component\HttpFoundation\Request;


class HomepageController extends AbstractController
{
	/**
     * @Route("/homepage", name="homepage")
     */
    public function homepage(Request $request)
    {
        $upload = new Upload();
        $form = $this->createForm(UploadType::class, $upload);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $file = $upload->getName();
            $fileName = md5(uniqid()).'.'.$file->guessExtension();
            $file->move($this->getParameter('upload_directory'), $fileName);
            $upload->setName($fileName);

            $file = $upload->getName1();
            $fileName = md5(uniqid()).'.'.$file->guessExtension();
            $file->move($this->getParameter('upload_directory'), $fileName);
            $upload->setName1($fileName);

            return $this->redirectToRoute('homepage');
        }
        return $this->render('homepage/index.html.twig', array(
        	'form' => $form->createView(),
        ));
    }
}



