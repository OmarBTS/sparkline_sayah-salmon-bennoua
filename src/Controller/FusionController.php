<?php

namespace App\Controller;

use Symfony\Component\Routing\Annotation\Route;
use App\Form\UploadType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
Use App\Entity\Upload;
use Symfony\Component\HttpFoundation\Request;


class FusionController extends AbstractController
{
    public function read1()
    {
        $fichier = 'uploads/french-data.csv';
        $file = fopen($fichier, 'r');
        while($tab1[] = fgetcsv($file, 2000, ",")) ;
        fclose($file);
        return $tab1;
    }

    public function read2()
    {
        $fichier2 = 'uploads/german-data.csv';
        $file2 = fopen($fichier2, 'r');
        while($tab2[] = fgetcsv($file2, 2000, ","));
        fclose($file2);
        return $tab2;
    }

    /**
     * @Route ("/fusionseq", name="fusionseq")
     */
    public function fusionseq()
    {
        $frenchtab = FusionController::read1();
        $germantab = FusionController::read2();
        $filer = 'download/vide.csv';
        $filef = fopen($filer, 'w');
        foreach ($frenchtab as $values) {
            if ($values) {
                fputcsv($filef, $values, ",");
            }
            foreach ($germantab as $values) {
                if ($values) {
                    fputcsv($filef, $values, ",");
                }
            }

        }
        fclose($filef);
        return $this->redirectToRoute('fusion');
    }

    /**
     * @Route("/fusion", name="fusion")
     */
    public function download(){
        return $this->render('FusionController/dl.html.twig');
    }
}