# sparkline_sayah-salmon-bennoua
Projet SparkLine

Niveau : Deuxième année de BTS SIO SLAM


== Thème 

La société de service dans laquelle vous travaillez a comme client l’entreprise SPARK-LINE, createur de ligne de vêtements.
Cette société souhaite constituer un fichier client unique, au format CSV, à partir de deux fichiers d’exportation transmis par ses filiales Française et Allemande.Ces fichiers sont : french-client.csv (~3000 clients) et german-client.csv (~2000 clients).

Le fichier résultant sera composé d’un sous-ensemble des colonnes existantes (projection) et une sélection de lignes sera effectuée (sélection des personnes majeures uniquement).

Dans un second temps, le service R&D de la société SPARK-LINE souhaite obtenir ces données sous la forme d’une base de données relationnelle.
== Objectif

Au cours de ce projet nous avions plusieurs objectifs :

Lecture de fichiers csv

Fusion de deux fichiers au format csv

Gestion de l’upload et du download de fichier

Conception de la partie Model

Conception d’une fonction ETL


Création de statistiques en Javascript
Le README.adoc de votre projet hébergé sur GitLab.com fera office de rapport de projet.
Présentation de votre branche de tests unitaires couvrant :
-les fusions séquetielles et entrelacées, avec comme données d’entrée les fichiers small-french-client.csv et small-german-client.csv.
-les extractions de clients pour données incorrectes
-Opération de fusion en tant que service injecté

Les opérations liées à la fusion (des méthodes/fonctions) seront placées dans une classe à part, en vue d’une utilisation par injection de service dans un contrôleur.

* Mise au point de la logique applicative avec PHPUnit
* Notion de structure de données, recherche d'un élement dans une liste 
* Analyse des actions du joueur (fonction du nombre de cartes, aides à la décision)


Répartition des tâches

Pour la répartition des tâches, dans un premier temps, brice s’est occupé de la création du projet, nous nous somment occupé des recherches pour le projet (ETL, CSV, fusion de fichier …). Une fois les recherches faîtes et la création du projet, omar s’est occupé  de l’upload du projet. Pendant ce temps, esteban c’est occupé de la création des fonctions de lecture et de fusions des fichiers csv. Une fois les fonctions de fusion adapté à Symfony, brice c'est occupé du code download du fichier fusionné. Lorsque l’upload est terminé, pour finir omar c'est occupé du download. Une fois le download terminé, nous avons commit le projet


== Premiers éléments d'analyse

voici la class uploadtype celle ci nous sert a construire le formulaire d'upload

class UploadType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('name', FileType::class, array(
                'label' => 'CSV 1'
            ))
            ->add('name1', FileType::class, array(
                'label' => 'CSV 2'
            ))

            ->add('submit', SubmitType::class)
        ;
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Upload::class,
        ]);
    }
}

ensuite voici le code permettant l'upload du fichier selectionné dans le dossier voulu

class HomepageController extends AbstractController
{
    /**
     * @Route("/homepage", name="homepage")
     */
    public function homepage(Request $request)
    {
        $upload = new Upload();
        $form = $this->createForm(UploadType::class, $upload);

        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $file = $upload->getName();
            $fileName = md5(uniqid()).'.'.$file->guessExtension();
            $file->move($this->getParameter('upload_directory'), $fileName);
            $upload->setName($fileName);

            $file = $upload->getName1();
            $fileName = md5(uniqid()).'.'.$file->guessExtension();
            $file->move($this->getParameter('upload_directory'), $fileName);
            $upload->setName1($fileName);

            return $this->redirectToRoute('homepage');
        }
        return $this->render('homepage/index.html.twig', array(
            'form' => $form->createView(),
        ));
    }
}
 une fois le fichier selectioné upload nous allons nous occuper de la fusion

 class FusionController extends AbstractController
 {
     public function read1()
     {
         $fichier = 'uploads/french-data.csv';
         $file = fopen($fichier, 'r');
         while($tab1[] = fgetcsv($file, 2000, ",")) ;
         fclose($file);
         return $tab1;
     }

     public function read2()
     {
         $fichier2 = 'uploads/german-data.csv';
         $file2 = fopen($fichier2, 'r');
         while($tab2[] = fgetcsv($file2, 2000, ","));
         fclose($file2);
         return $tab2;
     }

     /
      * @Route ("/fusionseq", name="fusionseq")
      */
     public function fusionseq()
     {
         $frenchtab = FusionController::read1();
         $germantab = FusionController::read2();
         $filer = 'download/vide.csv';
         $filef = fopen($filer, 'w');
         foreach ($frenchtab as $values) {
             if ($values) {
                 fputcsv($filef, $values, ",");
             }
             foreach ($germantab as $values) {
                 if ($values) {
                     fputcsv($filef, $values, ",");
                 }
             }

         }
         fclose($filef);
         return $this->redirectToRoute('fusion');
     }

     /
      * @Route("/fusion", name="fusion")
      */
     public function download(){
         return $this->render('FusionController/dl.html.twig');
     }
 }
Ce code nous permet de realiser la lecture des fichier avec les fonctions read puis la fonction fusionseq va proceder a la fusion des deux fichier csv en un seul fichier
pour la fonction fusionseq la variable germantab et frenchtab recupere les donnees des fichier ensuite le fichier vide.csv et selectionné et la variable $filef ouvre le fichier pour pouvoir y inscrire les donnees

{% extends "base.html.twig" %}

{% block body %}
   <a href="/download/vide.csv" download="vide.csv">download</a>
{% endblock %}

pour finir voici le code permettant de telecharger le fichier fusionné des deux fichier csv

Conclusion

Pour conclure, notre projet permet la fusion de 2 fichiers csv avec le mode sequentiel et de le telecharger par la suite

== Livraison

Modalité de livraison (mode « binôme ») : Salmon Esteban,Bennoua Brice,Sayah Omar

Deadline dimanche 18 octobre 2020 00h00



